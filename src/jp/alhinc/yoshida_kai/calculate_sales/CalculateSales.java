package jp.alhinc.yoshida_kai.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		if(args.length!=1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		//マップで入れ物を二つ作る

		//支店定義ファイル読み込み処理
		BufferedReader br = null;

		if(!readFile(args[0],"branch.lst",branchNames, branchSales)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();

		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length ; i++) {
			String fileName = files[i].getName();     //ファイルネームの取得・get.Nameにはファイルとディレクトリを取り出す機能がある
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {//ファイルではなかった時のエラー処理。
				rcdFiles.add(files[i]);
				//売上ファイルの読み込み
			}
		}
		for( int i = 0; i < rcdFiles.size() - 1; i++) {

			File file1 = rcdFiles.get(i);
			File file2 = rcdFiles.get(i + 1);

			String file1Name = file1.getName();
			String file2Name = file2.getName();

			String file1Number = file1Name.substring(0,8);
			String file2Number = file2Name.substring(0,8);

			int former = Integer.parseInt(file1Number);
			int latter = Integer.parseInt(file2Number);

			if((latter - former) != 1) {
				System.out.println("売上ファイルが連番になっていません"); //連番のエラー処理
				return;
			}
		}
		for(int i =0; i < rcdFiles.size(); i++) {


			try {
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				ArrayList<String> fileContents = new ArrayList<>();

				String line = "";
				while((line = br.readLine()) != null) {
					fileContents.add(line);
					//売上ファイルを読み込んでリスト化
				}

				String fileName = rcdFiles.get(i).getName();

				//行数チェック
				if(fileContents.size() != 2) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}
				//売上ファイルの金額が数値になっているかの確認
				if(!fileContents.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//支店コードの存在確認
				if(!branchNames.containsKey(fileContents.get(0))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}
				String branchCode = fileContents.get(0);

				long fileSale = Long.parseLong(fileContents.get(1));
				Long saleAmount = branchSales.get(branchCode) + fileSale;

				int amount = 0;
				String saleAmounts = String.valueOf(saleAmount);
				amount = saleAmounts.length();
				if(amount > 10) {
					System.out.println("金額が十桁を超えました");  //集計金額が10桁を超えた場合のエラー処理
				}

				branchSales.put(branchCode, saleAmount);
				//支店ごとの売上金額を集計し、マップに入れる

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
						//例外処理
					}
				}
			}
		}

		if(!writeFile(args[0],"branch.out",branchNames,branchSales)) {
				return;
		}
	}
	private static boolean writeFile(String path,String fileName,Map<String, String> branchNames,Map<String, Long> branchSales) {

		BufferedWriter bw = null;
		try {
			File file = new File(path,fileName);
			bw = new BufferedWriter(new FileWriter(file));

			for(String key : branchNames.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean readFile(String path,String fileName,Map<String, String> branchNames,Map<String, Long> branchSales) {

		//支店定義ファイル読み込み処理
		BufferedReader br = null;
		try {
			File file = new File(path,fileName);
			if(!file.exists()) {
				System.out.println("支店定義ファイルがありません");
				return false;
			}
			br = new BufferedReader(new FileReader(file));

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if((items.length !=2) || (!items[0].matches("^[0-9]{3}$"))){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
				//ファイルを読み込んでマップの中にそれぞれの値を入れた
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
					//例外処理
				}
			}
		}
		return true;
	}
}
